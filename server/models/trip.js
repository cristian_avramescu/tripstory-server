'use strict';

module.exports = function(Trip) {
    Trip.observe('before save', function setDate(ctx, next) {
        if(ctx.instance) {
            ctx.instance.creationDate = new Date();
            ctx.instance.lastModifiedDate = new Date();
        } else {
            ctx.data.lastModifiedDate = new Date();
        }
        next();      
    });
};
