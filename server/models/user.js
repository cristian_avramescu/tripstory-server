'use strict';
var shortid = require('shortid');

module.exports = function (User) {
    User.observe('after save', function (ctx, next) {
        if(ctx.instance) {
            var container = User.app.models.ImageContainer;
            container.createContainer({name: '' + ctx.instance.id}, function() {
                next();
            })
        } else {
            next();
        }
    });
};
