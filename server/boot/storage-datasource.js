var uniqid = require('uniqid');

module.exports = function (app) {
    app.datasources.Storage.connector.getFilename = function (file, req, res) {
        var fileName = file.name;
        var extension = fileName.substring(fileName.lastIndexOf('.'));
        return uniqid() + extension;
    }
}