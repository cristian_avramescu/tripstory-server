module.exports = function (app) {
    app.models.User.find(function (error, list) {
        if (error) {
            console.log(error);
            return;
        }
        list.forEach(function (element) {
            var containerModel = app.models.ImageContainer;
            var userId = element.getId().toString();
            containerModel.getContainer(userId, function (err, container) {
                if (err) {
                    if (err.status === 404) {
                        containerModel.createContainer({ name: userId }, function (e) {
                            if (e) {
                                console.log(e);
                            }
                        });
                    } else {
                        console.log(err);
                    }
                }
            });
        }, this);
    });
}