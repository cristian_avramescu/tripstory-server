module.exports = {
  db: {
    connector: 'mongodb',
    url: process.env.DB_URL
  },
  storage: {
    connector: "loopback-component-storage",
    provider: 'amazon',
    key: process.env.AWS_KEY,
    keyId: process.env.AWS_KEY_ID
  }
};